#!/usr/bin/env python
#
# Convert the CSV file output by quickDAQ to a WAV file
#
import sys
import wave
import numpy
from optparse import OptionParser

def fill_array(file):
    """
    Read a single column of ascii values from an input file and
    return as a floating-point array.
    """
    x = []
    for line in file:
        try:
            x.append(float(line.strip()))
        except ValueError:
            pass
    return numpy.array(x, dtype=numpy.float)

def read_header(infile, outfile):
    """
    Read the CSV header and write the WAV header. Return a dictionary
    of metadata.
    """
    columns = None
    params = {}
    for line in infile:
        fields = line.strip().split(',')
        if len(fields) == 1:
            break
        if fields[0] == 'PerChannelSamplingFreq :':
            freq = int(fields[1])
            outfile.setnchannels(1)
            outfile.setframerate(freq)
            outfile.setsampwidth(2)
        elif fields[0] == 'ChannelName':
            columns = fields[:]
        elif columns is not None:
            params = dict(zip(columns, fields))
            columns = None
    return params

def main():
    """
    %prog [options] infile

    Convert CSV file from quickDAQ to a WAV format audio file.
    """
    parser = OptionParser(usage=main.__doc__)
    parser.add_option("-o", "--output",
                      default="quickdaq.wav",
                      type="string",
                      dest="output",
                      help="output file name [default %default]")
    opts, args = parser.parse_args()
    try:
        file = open(args[0], 'r')
    except IndexError:
        file = sys.stdin
        
    v = []
    w = wave.open(opts.output, 'wb')
    params = read_header(file, w)
    scale = 1./float(params['DataScale'])
    v = fill_array(file)*scale
    iv = v.astype(numpy.int16)
    w.writeframes(iv.tostring())
    
if __name__ == '__main__':
    main()
