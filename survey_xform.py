#!/usr/bin/env python
#
"""
%prog Tx,Ty rotate [infile]

Transform the coordinates in a survey file by translation and/or rotation. The
survey data is read from INFILE or standard input. The new survey file is written
to standard output.
"""
import numpy
import math
import sys
from optparse import OptionParser

def write_file(outfile, coords):
    for i, p in enumerate(coords):
        x = p[0, 0]
        y = p[1, 0]
        outfile.write(' {0}, H{0}, {1:.3f}, {2:.3f}, 100,  Tracking Hyd\r\n'.format(i+1, x, y))

def read_file(infile):
    coords = []
    for line in infile:
        p = [float(c) for c in line.strip().split(',')[2:4]]
        p.append(1.)
        coords.append(numpy.matrix(p).T)
    return coords

def main():
    parser = OptionParser(__doc__)
    opts, args = parser.parse_args()

    if len(args) < 2:
        parser.error('Missing arguments')

    translate = numpy.matrix(numpy.eye(3))
    rotate = numpy.matrix(numpy.eye(3))

    tx, ty = [float(a) for a in args[0].split(',')]
    translate[0, 2] = tx
    translate[1, 2] = ty

    theta = math.radians(float(args[1]))
    s = math.sin(theta)
    c = math.cos(theta)
    rotate[0, 0] = c
    rotate[0, 1] = -s
    rotate[1, 0] = s
    rotate[1, 1] = c
    
    # Create the transform matrix, translation followed by rotation.
    A = rotate*translate

    try:
        infile = open(args[2], 'r')
    except IndexError:
        infile = sys.stdin
    except IOError:
        sys.stderr.write('Error opening input file: {0}\n'.format(args[2]))
        sys.exit(1)

    new_coords = []
    coords = read_file(infile)
    for p in coords:
        new_coords.append(A*p)

    write_file(sys.stdout, new_coords)

if __name__ == '__main__':
    main()
