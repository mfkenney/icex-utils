#!/usr/bin/env python
#
# Plot counter values from a Timers file.
#
import sys
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import time

COLORS = ['red', 'green', 'blue', 'magenta', 'cyan', 'yellow', 'chartreuse', 'crimson']

SIZE = 20, 40

def to_seconds(dt_str):
    """
    Convert a date/time string of the form MM/DD/YYYY HH:MM:SS
    to seconds since 1/1/1970.
    """
    return time.mktime(time.strptime(dt_str, '%m/%d/%Y %H:%M:%S'))

def process(infile, freq=1):
    """
    Process the Track log file and return a tuple of the following arrays:

      times  -- timestamps 
      counts -- counter values
      hydros -- hydrophone numbers
      sizes  -- symbol sizes (larger for 10 sec data)
    """
    times = []
    counts = []
    hydros = []
    sizes = []
    start = (freq - 1)*8 + 2
    end = start + 8
    for line in infile:
        f = line.strip().split()
        t = to_seconds('%s %s' % (f[0], f[1]))
        counts.extend([float(e) for e in f[start:end]])
        times.extend([t]*8)
        hydros.extend(range(1, 9))
        if (t % 10) == 0:
            sizes.extend([SIZE[1]]*8)
        else:
            sizes.extend([SIZE[0]]*8)
    return np.array(times), np.array(counts), np.array(hydros), np.array(sizes)

def make_scatter_plot(ax, times, counts, hydros, sizes, hydro_list):
    """
    Create a set of scatter plots, one for each hydrophone listed in hydro_list.

    @param ax: plot Axes
    @param times: array of timestamp values
    @param counts: array of counter values
    @param hydros: array of hydrophone numbers
    @param sizes: array of plot symbol sizes
    """
    handles = []
    labels = []

    for hydro in hydro_list:
        # mask for this hydrophone
        h_mask = hydros==hydro
        x = times[h_mask]
        y = counts[h_mask]
        sz = sizes[h_mask]
        color = COLORS[hydro-1]

        # Remove all entries with negative counter values (missing data)
        mask = y>0
	try:
            handles.append(
                ax.scatter(x[mask], y[mask]*0.01,
                           c=color, s=sz[mask], alpha=0.70))
            labels.append(r'$Hydro_%d$' % hydro)
	except:
	    pass
    if handles:
        ax.legend(handles, labels)
    
try:
    infile = open(sys.argv[1], 'r')
except IndexError:
    infile = sys.stdin

try:
    freq = int(sys.argv[2])
except IndexError:
    freq = 1
    
times, counts, hydros, sizes = process(infile, freq=freq)

# Record the start time and adjust the timestamps to be relative to
# this value.
start_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(times[0]))
times = times - times[0]

# Plot counters from first quad
fig = plt.figure()
ax = fig.add_subplot(211)

make_scatter_plot(ax, times, counts, hydros, sizes, range(1, 5))

ax.set_ylabel(r'Counter Value [ms]', fontsize=12)
ax.set_title(r'Quad 1')
ax.grid(True)

# Plot counters from second quad
ax = fig.add_subplot(212)

make_scatter_plot(ax, times, counts, hydros, sizes, range(5, 9))

ax.set_xlabel(r'Seconds since %s' % start_time, fontsize=12)
ax.set_ylabel(r'Counter Value [ms]', fontsize=12)
ax.set_title(r'Quad 2')
ax.grid(True)

plt.show()
