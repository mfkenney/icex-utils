#!/bin/bash
#
# Given a file of GPS positions (a track) in any format supported by Gpsbabel, output a
# file of ranges and bearings from a reference position.
#
PROG="$0"

usage ()
{
    cat<<EOF 1>&2
Usage: $PROG filetype filename reflat reflon

Time, bearing, and range (in meters) is written to stdout.
EOF
}

if [ $# -lt 4 ]
then
    usage
    exit 1
fi

intype="$1"
infile="$2"
reflat="$3"
reflon="$4"

gpsbabel -i $intype -f "$infile" -o xcsv,style=$HOME/.gpsbabel/gpslog.style -F /dev/stdout |\
while read tstamp lat lon
do
    result=$(echo "$reflat $reflon $lat $lon" | geod -I +ellps=WGS84 -f "%.1f" -F "%.1f")
    set -- $result
    # Result is forward-azimuth, back-azimuth, range
    echo "$tstamp $1 $3"
done
