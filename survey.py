#!/usr/bin/env python
#
from datetime import datetime
from matplotlib.mlab import csv2rec
from matplotlib.dates import date2num
import numpy
import math
import yaml
import os
from functools import partial
import sys
import textwrap

def read_counters(infile):
    """
    Read a tracking-range counters file into a Numpy record array.
    """
    def datestr2num(s):
        return date2num(datetime.strptime(s, '%m/%d/%Y %H:%M:%S'))
    columns = ('time', 'f1q1h1', 'f1q1h2', 'f1q1h3', 'f1q1h4',
               'f1q2h1', 'f1q2h2', 'f1q2h3', 'f1q2h4',
               'f2q1h1', 'f2q1h2', 'f2q1h3', 'f2q1h4',
               'f2q2h1', 'f2q2h2', 'f2q2h3', 'f2q2h4')
    missingd = dict([(c, '-1') for c in columns if c != 'time'])
    converterd = {}
    converterd['time'] = datestr2num
    return csv2rec(infile, names=columns,
                   converterd=converterd,
                   missingd=missingd,
                   use_mrecords=True,
                   delimiter='\t')

def travel_times(trkfile, transmitter, tfilter, quad=1):
    """
    Return an array of tuples where each element is a hydrophone number,
    average travel-time (ticks) pair.
    """
    counters = read_counters(trkfile)
    tsteps = {}
    indicies = []
    # Save only the first value for each time step.
    for i, rec in numpy.ndenumerate(counters):
        if rec[0] not in tsteps:
            tsteps[rec[0]] = 1
            indicies.append(i)
    counters = counters[indicies]
    recvs = [i for i in range(1, 5) if i != transmitter]
    fields = ['f1q{0:d}h{1:d}'.format(quad, r) for r in recvs]
    tt = []
    for r, f in zip(recvs, fields):
        x = counters[f]
        sys.stderr.write('{0:d} => {1:d}: '.format(transmitter, r))
        # x[~mask] selects all of the non-empty values.
        tt.append((r, tfilter(x[~x.mask])))
    return tt

def filtered_mean(x, n=1.):
    """
    Compute a mean value after removing all values outside of
    n standard deviations from the mean.
    """
    mean = numpy.mean(x)
    std = numpy.std(x)
    sys.stderr.write('mean={0:.1f} ticks, std={1:.1f} ticks\n'.format(mean, std))
    xp = numpy.ma.masked_outside(x, mean-std*n, mean+std*n)
    return numpy.mean(xp[~xp.mask])

def filtered_median(x, n=1.):
    """
    Compute a median value after removing all values outside of
    n standard deviations from the mean.
    """
    mean = numpy.mean(x)
    std = numpy.std(x)
    sys.stderr.write('mean={0:.1f} ticks, std={1:.1f} ticks\n'.format(mean, std))
    xp = numpy.ma.masked_outside(x, mean-std*n, mean+std*n)
    return numpy.median(xp[~xp.mask])

def build_matrix(track_files, tfilters={}, offset=0.):
    """
    Create a matrix of travel times between hydrophones:

    M[i,j] := travel time in seconds from hydrophone i+1 to hydrophone j+1
    """
    tts = numpy.zeros((4, 4))
    for tnum, filename in track_files:
        tt = travel_times(filename, tnum, tfilters.get(tnum, numpy.median))
        for rnum, val in tt:
            tts[tnum-1, rnum-1] = val/1e5 - offset
    return tts

def survey(tt, sspeed=1500, duplicate=False):
    """
    Using the travel-time matrix, determine a best-fit solution for the hydrophone
    locations. The origin (0, 0) is placed at hydrophone 4 and the Y-axis is parallel
    to the line between hydrophones 1 and 2.

    If duplicate is true, the coordinates for the second quad (hydrophones 5-8) are
    returned as duplicates of 1-4 otherwise they are returned as all zeros.
    """
    # Compute the average travel time between each hydrophone pair.
    d12 = (tt[0, 1] + tt[1, 0])/2.
    d23 = (tt[1, 2] + tt[2, 1])/2.
    d31 = (tt[2, 0] + tt[0, 2])/2.
    d14 = (tt[0, 3] + tt[3, 0])/2.
    d24 = (tt[1, 3] + tt[3, 1])/2.
    d34 = (tt[2, 3] + tt[3, 2])/2.

    d = (d23**2 - d31**2 + d12**2)/2/d12
    e = d12 - d
    a = math.sqrt(d31**2 - e**2)
    d9 = (d24**2 - d14**2 + d12**2) /2./d12
    e9 = d12 - d9
    a9 = math.sqrt(d14**2 - e9**2)
    e8 = e - e9

    coords = numpy.zeros((8, 2), numpy.float)
    # Hydrophone 1
    x = -a9*sspeed
    y = (e - e8)*sspeed
    coords[0, 0] = x
    coords[0, 1] = y

    # Hydrophone 2
    x = -a9*sspeed
    y = (-d - e8)*sspeed
    coords[1, 0] = x
    coords[1, 1] = y

    # Hydrophone 3
    x = (a - a9)*sspeed
    y = -e8*sspeed
    coords[2, 0] = x
    coords[2, 1] = y

    if duplicate:
        for i in range(4, 8):
            j = i - 4
            coords[i, 0] = coords[j, 0]
            coords[i, 1] = coords[j, 1]

    return coords

def write_survey_file(coords, filename='survey.xyz'):
    outf = open(filename, 'w')
    for i in range(1, 9):
        outf.write(' {0}, H{0}, {1:.3f}, {2:.3f}, 100,  Tracking Hyd\r\n'.format(i, coords[i-1, 0],
                                                                                 coords[i-1, 1]))
    outf.close()

def main():
    """
    %prog cfgfile
    """
    try:
        cfgfile = sys.argv[1]
    except IndexError:
        sys.stderr.write(textwrap.dedent(main.__doc__))
        sys.exit(1)

    cfg = yaml.load(open(cfgfile, 'r'))
    files = [(t, os.path.join(cfg['input']['directory'], f)) for t, f in cfg['input']['files']]
    filters = {}
    if 'filters' in cfg:
        for filt_name in cfg['filters']:
            filter_func = eval(filt_name)
            filter_desc = cfg['filters'][filt_name]
            if 'args' in filter_desc:
                filter_func = partial(filter_func, **filter_desc['args'])
            for tnum in filter_desc['transmitters']:
                filters[tnum] = filter_func

    tt = build_matrix(files, tfilters=filters, offset=cfg['integration_time'])
    print tt
    xy = survey(tt, sspeed=cfg['sound_speed'], duplicate=cfg['duplicate'])
    print xy
    write_survey_file(xy)

if __name__ == '__main__':
    main()
