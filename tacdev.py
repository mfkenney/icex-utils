#!/usr/bin/env python
#
# Convert ICEX-11 X-Y data to lat-lon
#
from trackxy import parse_xy, target_labels
import pyproj
import math
from datetime import datetime

class Projection(object):
    def __init__(self, lat0, lon0, az):
        azimuth = math.radians(360. - az)
        self._c = math.cos(azimuth)
        self._s = math.sin(azimuth)
        self.proj = pyproj.Proj(proj='tmerc', ellps='WGS84',
                                units='us-yd',
                                lat_0=lat0,
                                lon_0=lon0)

    def __call__(self, x, y):
        east = x*self._c - y*self._s
        north = y*self._c + x*self._s
        lon, lat = self.proj(east, north, inverse=True)
        return lat, lon

def t_hash(t):
    return int((t//10)*10)

def convert(indata, inref, outdata, target='f1q1'):
    proj = None
    t0 = 0
    for line in indata:
        rec = parse_xy(line)
        t = t_hash(rec['time'])
        if t0 < t:
            for rline in inref:
                t0, lat0, lon0, az = [float(e) for e in rline.split(',')]
                if t0 >= t:
                    proj = Projection(lat0, lon0, az)
                    break
        if proj:
            x, y = rec[target]
            if not (math.isnan(x) or math.isnan(y)):
                lat, lon = proj(x, y)
                tstamp = datetime.utcfromtimestamp(rec['time'])
                outdata.write('{0},{1},{4:.1f},{5:.1f},{2:.6f},{3:.6f}\n'.format(tstamp.isoformat(),
                                                                                 (t % 20) and 'odd' or 'even',
                                                                                 lat,
                                                                                 lon, x, y))
        else:
            raise Exception('Unexpected end of data')
        

def main():
    """
    tacdev.py <reffile> [xyfile]

    Convert ICEX X-Y position data to lat-lon and write to standard output
    in CSV format. <reffile> is a CSV file containing, the latitude and
    longitude of the grid origin and the true azimuth of the Y-axis.
    """
    import sys, textwrap

    try:
        inref = open(sys.argv[1], 'r')
    except IndexError:
        sys.stderr.write(textwrap.dedent(main.__doc__))
        sys.exit(1)

    try:
        indata = open(sys.argv[2], 'r')
    except IndexError:
        indata = sys.stdin

    convert(indata, inref, sys.stdout)

if __name__ == '__main__':
    main()
