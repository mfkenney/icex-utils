#!/usr/bin/env python
#
# Receive icex position signals over the D-bus
#
import dbus
import dbus.service
import gobject
from dbus.exceptions import DBusException
import dbus.mainloop.glib
import time
import sys
from optparse import OptionParser

def timestr(t):
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(t))

def make_alt_signal_handler(interval):
    labels = {0 : 'EVEN', interval : 'ODD'}
    def alt_signal_handler(t, x, y):
        print '%s %.0f %.0f\t%s' % (timestr(t), x, y, labels.get(t%(interval*2), ''))
    return alt_signal_handler

def signal_handler(t, x, y):
    print '%s %.0f %.0f' % (timestr(t), x, y)
    
def main():
    """
    %prog [options] freq quad

    Listen for XY updates from the specified frequency (1 or 2) and
    quad (1 or 2)
    """
    parser = OptionParser(usage=main.__doc__)
    parser.add_option("-a", "--alternate",
                      default=None,
                      type="int",
                      dest="alternate",
                      metavar="N",
                      help="use alternate N second scheme")
    opts, args = parser.parse_args()
    
    try:
        freq = int(args[0])
        quad = int(args[1])
    except IndexError:
        parser.error('Missing arguments')
        
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    bus = dbus.SessionBus()
    if opts.alternate:
        handler = make_alt_signal_handler(opts.alternate)
    else:
        handler = signal_handler
    bus.add_signal_receiver(handler, dbus_interface='icex.IPositions',
                            signal_name='f%dq%d' % (freq, quad))
    loop = gobject.MainLoop()
    try:
        loop.run()
    except KeyboardInterrupt:
        sys.exit(0)

if __name__ == '__main__':
    main()
