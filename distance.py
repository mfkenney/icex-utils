#!/usr/bin/env python
#
# Calculate the distance between two GPS points
#
# Usage: distance.py  lat1,lon1 lat2,lon2
#
import geopy
from geopy.point import Point
from geopy.distance import VincentyDistance as VD
import Geo.geo

def main(args):
    try:
        p0 = Point(args[0])
        p1 = Point(args[1])
    except IndexError:
        print >> sys.stderr, "Usage: distance.py lat1,lon1 lat2,lon2"
        sys.exit(1)
        
    d1 = VD(p0, p1)
    d2 = Geo.geo.dist(p0.latitude, p0.longitude,
                      p1.latitude, p1.longitude)
    print d1.kilometers, d2

if __name__ == '__main__':
    import sys
    main(sys.argv[1:])
