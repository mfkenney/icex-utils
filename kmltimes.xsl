<?xml version="1.0" encoding="utf-8"?>
<!-- arch-tag: 218dfa20-58e3-463d-ad5e-c093676e8b5d
     (do not change this comment) -->
<!--
    Stylesheet to extract the time range of a KML track file
-->
<xsl:stylesheet version="1.0"
  xmlns:k="http://www.opengis.net/kml/2.2"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:gx="http://www.google.com/kml/ext/2.2"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text"/>

  <xsl:template match="/">
    <xsl:apply-templates select="k:kml"/>
  </xsl:template>

  <xsl:template match="k:kml">
    <xsl:apply-templates select="k:Document"/>
  </xsl:template>

  <xsl:template match="k:Document">
    <xsl:apply-templates select="k:LookAt"/>
  </xsl:template>

  <xsl:template match="k:LookAt">
    <xsl:apply-templates select="gx:TimeSpan"/>
  </xsl:template>

  <xsl:template match="gx:TimeSpan">
    <xsl:value-of select="k:begin"/>
    <xsl:text>_</xsl:text>
    <xsl:value-of select="k:end"/>
    <xsl:text>&#10;</xsl:text>
  </xsl:template>

</xsl:stylesheet>
