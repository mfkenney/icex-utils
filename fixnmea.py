#!/usr/bin/env python
#
# Fix bad records in the NMEA files from the ICEX gps logger.
#
import sys

#
# The bad records are written as a partial record and then a full record
# with no intervening line-feed.
#
def fix_record(line):
    fields = line.split(',', 1)
    if len(fields) == 11:
        return line
    marker = fields[0]
    i = 1
    n = len(line)
    j = i + len(marker)
    while j < n:
        if marker == line[i:j]:
            return line[i:]
        i += 1
        j += 1

    return line

def main():
    infile = open(sys.argv[1], 'r')
    for line in infile:
        output = fix_record(line.rstrip())
        print "$GPRMC,"+output

if __name__ == '__main__':
    main()
