#!/usr/bin/env python
#
# Use the grid-origin GPS positions to calculate floe velocity estimates.
#
import pyproj
import sys
from datetime import datetime

wgs_geod = pyproj.Geod(ellps='WGS84')

def t_hash(t):
    return int((t//10)*10)

def calc_velocity(p1, p0):
    t0, lat0, lon0 = p0[0:3]
    t1, lat1, lon1 = p1[0:3]
    fwd, back, dist = wgs_geod.inv(lon0, lat0, lon1, lat1)
    return t1, dist/(t1 - t0), fwd
    
def positions(infile, interval=3600):
    # Skip the first line
    line = infile.next()
    for line in infile:
        t0, lat0, lon0, az = [float(e) for e in line.split(',')]
        if (t_hash(t0) % interval) == 0:
            yield t0, lat0, lon0

def vel_estimate(datafile, interval=3600):
    infile = open(datafile, 'r')
    p0 = None
    for p in positions(infile, interval):
        if p0 is None:
            p0 = p
            continue
        v = calc_velocity(p0, p)
        p0 = p
        yield v

def main():
    for t, vel, az in vel_estimate(sys.argv[1]):
        print '%s,%.3f,%.1f' % (datetime.utcfromtimestamp(t).isoformat(' '),
                                vel,
                                az)

if __name__ == '__main__':
    main()
