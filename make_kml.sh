#!/bin/bash
#
# Convert the NMEA format GPS log to KML for display in Google Earth. The
# log is first processed with 'fixnmea.py' to remove corrupted records and
# to restore the $GPRMC field.
#

if [ $# -lt 2 ]
then
    echo "Usage: $(basename $0) infile outfile"
    exit 1
fi

infile="$1"
outfile="$2"

fixnmea.py "$infile" | gpsbabel -i nmea -f /dev/stdin \
  -t -o kml,points=0,track=1,trackdata=0,trackdirection=1 -F "$outfile"
