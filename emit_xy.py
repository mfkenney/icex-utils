#!/usr/bin/env python
#
# Read XY values from a serial port as output by the Track program and emit
# them as signals on the D-bus.
#
import dbus
import dbus.service
import dbus.mainloop.glib
import serial
import time
import signal
from optparse import OptionParser
from datetime import datetime
from dateutils.tz import tzutc

class Emitter(dbus.service.Object):
    def __init__(self, bus):
        dbus.service.Object.__init__(self, bus, '/Positions')
        self.bus = bus

    @dbus.service.signal(dbus_interface='icex.IPositions',
                         signature='sa(sdd)')
    def dispatch(self, t, positions):
        pass

def make_timestamp(dt_str):
    """
    Convert a date/time string of the form MM/DD/YYYY HH:MM:SS
    to ISO format.
    """
    dt = datetime.strptime(dt_str, '%m/%d/%Y %H:%M:%S').replace(tzinfo=tzutc())
    return dt.isoformat()

def read_records(input, eol='\n'):
    """
    Generator to return the next XY record from the input port
    """
    buf = []
    while True:
        c = input.read(1)
        if not c:
            continue
        buf.append(c)
        if c == eol:
            if buf[0] != 'X':
                yield ''.join(buf)
            buf = []
            
def split_record(rec):
    """
    Split a timer record string into a tuple consisting of a timestamp
    and a list of XY values with associated labels.
    """
    labels = ['F1Q1', 'F1Q2', 'F2Q1', 'F2Q2']
    fields = rec.strip().split()
    t = make_timestamp('%s %s' % tuple(fields[0:2]))
    xy = zip(labels, fields[2::2], fields[3::2])
    return t, xy

def process_records(source, emitter):
    for rec in read_records(source):
        try:
            timestamp, xy = split_record(rec)
            emitter.dispatch(timestamp, [(l, float(x), float(y)) for l, x, y in xy])
        except (ValueError, IndexError):
            pass

def handler(*args):
    raise KeyboardInterrupt

def main():
    """
    %prog [options]

    DBus service to read tracking range XY values from a serial port and
    distribute them via DBus signals.
    """
    parser = OptionParser(main.__doc__)
    parser.set_defaults(devname='/dev/ttyS0', baudrate=9600)
    parser.add_option('-d', '--devname',
                      dest='devname',
                      type="string",
                      help='serial port device name (%default)',
                      metavar='NAME')
    parser.add_option('-b', '--baudrate',
                      dest='baudrate',
                      type="int",
                      help='serial port baud rate (%default)',
                      metavar='BPS')

    opts, args = parser.parse_args()

    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    bus = dbus.SessionBus()
    dbus.service.BusName("icex.Positions", bus)
    source = serial.Serial(opts.devname, baudrate=opts.baudrate, timeout=5)
    emitter = Emitter(bus)

    # Catch SIGTERM
    signal.signal(signal.SIGTERM, handler)

    try:
        process_records(source, emitter)
    except KeyboardInterrupt:
        return 0

if __name__ == '__main__':
    main()
