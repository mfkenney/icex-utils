#!/bin/bash
#
# Given a file of X-Y positions on the APLIS grid, output a file of latitude and longitude.
#
PROG="$0"

usage ()
{
    cat<<EOF 1>&2
Usage: $PROG filename lat0,lon0,az

Input file contains X, Y, and time. Time, latitude, and longitude (in
degrees) is written to stdout. lat0, lon0, and az define the grid projection.
EOF
}

if [ $# -lt 2 ]
then
    usage
    exit 1
fi

infile="$1"
params="$2"

save_ifs="$IFS"
IFS=","
set -- $params
IFS="$save_ifs"
lat0="$1"
lon0="$2"
az="$3"

#
# Rotate the grid coordinates to align the Y-axis with true north then perform an
# inverse Transverse Mercator projection.
#
gawk -v az="$az" 'BEGIN {az_r = (360 - az)*atan2(1, 1)*4./180.;\
                         c_a = cos(az_r); s_a = sin(az_r);} \
              function calc_e(x, y) { return x*c_a - y*s_a }\
              function calc_n(x, y) { return y*c_a + x*s_a }\
              {printf "%.2f %.2f %s\n", calc_e($1,$2), calc_n($1,$2), $3}' $infile |\
  invproj +proj=tmerc +ellps=WGS84 +units=us-yd +lat_0=$lat0 +lon_0=$lon0 -f "%.6f" |\
  while read lon lat timestamp
  do
      echo "$timestamp $lat $lon"
  done
