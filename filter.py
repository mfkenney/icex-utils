#!/usr/bin/env python
#
# Example Kalman filter for GPS positions. 
#
import numpy
import sys
import math

# The constants a and b are from http://users.erols.com/dlwilson/gpshdop.htm
def rms_horiz_error(hdop, a=3.07, b=3.57):
    return math.sqrt((a*hdop)**2 + b**2)

METERS_PER_DEG = 60*1852.

HDOP = 1.5
h_error = rms_horiz_error(HDOP)

# Position estimate
xhat = numpy.matrix([[0.], [0.]])
# Position measurement
z = numpy.matrix([[0.], [0.]])
# Measurement noise covariance (function of HDOP)
R = numpy.matrix(numpy.eye(2))
# Process noise covariance (constant)
Q = numpy.matrix(numpy.eye(2)*1.0e-5)
# Error estimate
P = numpy.matrix(numpy.zeros((2, 2)))
# Filter gain
K = numpy.matrix(numpy.zeros((2, 2)))
# Identity matrix
I = numpy.matrix(numpy.eye(2))
# State change matrix
A = numpy.matrix(numpy.eye(2))

infile = open(sys.argv[1], 'r')
t, lat, lon = infile.next().strip().split()
xhat[0,0] = float(lon)
xhat[1,0] = float(lat)
# Convert horizontal error from meters to degrees
R[1,1] = (h_error/METERS_PER_DEG)**2
R[0,0] = (h_error/(METERS_PER_DEG*math.cos(math.radians(xhat[1,0]))))**2

for line in infile:
    t, lat, lon = line.strip().split()
    xhatbar = A*xhat
    Pbar = A*P*A.T + Q
    s = Pbar + R
    K = Pbar*s.I
    z[0,0] = float(lon)
    z[1,0] = float(lat)
    R[0,0] = (h_error/(METERS_PER_DEG*math.cos(math.radians(z[1,0]))))**2
    xhat = xhatbar + K*(z - xhatbar)
    P = (I - K)*Pbar
    print '{0} {1:.6f} {2:.6f}'.format(t, xhat[1,0], xhat[0,0])
