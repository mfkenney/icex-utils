#!/usr/bin/env python
#
# $Id$
#
# Functions to work with ICEX tracking data
#
import numpy
import math
#import pylab
from datetime import datetime
#from matplotlib.patches import Ellipse

rand = numpy.random.rand

def parse_record(rec):
    """
    Parse a tracking record of the form:

    date time T[i, j, k] ...

    i := hydrophone index
    j := target index
    k := hydrophone array index
    """
    fields = rec.split()
    tstamp = datetime.strptime(' '.join(fields[0:2]), '%m/%d/%Y %H:%M:%S')
    times = numpy.array([float(t) for t in fields[2:34]]).reshape(2, 4, 4)
    return tstamp, times


def hyper_xyz(times, hydro_xy, sound_speed, r1est=0.):
    """
    @param times: 1x4 array of travel times (seconds)
    @param hydro_xy: 4x3 array of hydrophone locations.
    @param sound_speed: 1x4 array of avg path sound speed.
    """
    origin = hydro_xy[0,:]
    hx = hydro_xy[:,0] - origin[0]
    hy = hydro_xy[:,1] - origin[1]
    hz = hydro_xy[:,2] - origin[2]

    dt = times - times[0]
    dv = (sound_speed - sound_speed[0])/sound_speed[0]
    d = sound_speed*dt + r1est*dv
    e = ((hx*hx) + (hy*hy) + (hz*hz) - (d*d))/2.
    
    f = numpy.ones(3)
    f[0] = (hx[2]*hy[3]) - (hx[3]*hy[2])
    f[1] = (hx[3]*hy[1]) - (hx[1]*hy[3])
    f[2] = (hx[1]*hy[2]) - (hx[2]*hy[1])
    g = numpy.dot(d[1:4], f)
    target1 = numpy.zeros(3)
    target2 = numpy.zeros(3)
    
    if abs(g) < 1.0e-32:
        return False, target1, target2, -1, -1

    p1 = -numpy.dot(hz[1:4]*f)/g
    p2 = numpy.dot(e[1:4]*f)/g
    h1 = e[1] - (d[1] * p2)
    h2 = hz[1] + (d[1] * p1)
    if abs(f[2]) < abs(f[1]):
        h3 = e[3] - d[3]*p2
        h4 = z[3] + d[3]*p1
        p3 = ((hx[1]*h4) - (hx[3]*h2))/f[1]
        p4 = -((hx[1]*h3) - (hx[3]*h1))/f[1]
        p5 = -((hy[1]*h4) - (hy[3]*h2))/f[1]
        p6 = ((hy[1]*h3) - (hy[3]*h1))/f[1]
    else:
        h3 = e[2] - (d[2]*p2)
        h4 = hz[2] + (d[2]*p1)
        p3 = -((hx[1]*h4) - (hx[2]*h2))/f[2]
        p4 = ((hx[1]*h3) - (hx[2]*h1))/f[2]
        p5 = ((hy[1]*h4) - (hy[2]*h2))/f[2]
        p6 = -((hy[1]*h3) - (hy[2]*h1))/f[2]
        
    a = 1. - p1**2 + p3**2 + p5**2
    ba = -((p1*p2) - (p3*p4) - (p5*p6))/a
    ca = -p2**2 - p4**2 - p6**2/a
    quant = ba**2 - ca
    if quant < 0:
        quant = 0.
    root = math.sqrt(quant)
    za = -ba - root
    zb = -ba + root
    r1 = p1*za + p2
    pet1 = times[0] - (r1/sound_speed[0])
    target1[0] = p5*za + p6
    target1[1] = p3*za + p4
    target1[2] = za
    r2 = p1*zb + p2
    pet2 = times[0] - (r2/sound_speed[0])
    target2[0] = p5*zb + p6
    target2[1] = p3*zb + p4
    target2[3] = zb
    return True, target1+origin, target2+origin, pet1, pet2

def spherical_xyz(times, hydro_xy, sound_speed):
    """
    Calculate the target location using a 3d-spherical algorithm.
    
    @param times: 1x3 array of travel times (seconds)
    @param hydro_xy: 3x3 array of hydrophone locations (1 row per hydrophone)
    @param sound_speed: 1x3 array of avg path sound speed.
    @return two-element tuple of target xyz locations (1x3 arrays).
    """
    
    a1 = hydro_xy[0,:] - hydro_xy[1,:]
    a2 = hydro_xy[0,:] - hydro_xy[2,:]
    a3 = numpy.zeros(3)
    a3[0] = a1[1]*a2[2] - a1[2]*a2[1]
    a3[1] = a1[2]*a2[0] - a1[0]*a2[2]
    a3[2] = a1[0]*a2[1] - a1[1]*a2[0]
    ss = numpy.zeros(3)
    r2 = (sound_speed*times)**2
    ss[0] = numpy.dot(a1, a1)
    ss[1] = numpy.dot(a2, a2)
    ss[2] = numpy.dot(a3, a3)
    s12 = numpy.dot(a1, a2)
    target1 = numpy.zeros(3)
    target2 = numpy.zeros(3)
    if abs(ss[2]) < 1.0e-32:
        return False, target1, target2
    u = ((r2[1] - r2[0]) - ss[0])/2.
    v = ((r2[2] - r2[0]) - ss[1])/2.
    alpha = (u*ss[1] - v*s12)/ss[2]
    beta = (v*ss[0] - u*s12)/ss[2]
    quant = ((r2[0] - alpha*u) - beta*v)/ss[2]
    if quant < 0:
        quant = 0.

    gamma = math.sqrt(quant)
    temp = hydro_xy[0,:] + alpha*a1 + beta*a2
    target1 = temp + gamma*a3
    target2 = temp - gamma*a3
    return True, target1, target2

def track_target(target, times, offsets, hydro_xy):
    """
    @param target: target index (0-3)
    @param times: 2x4x4 array of travel times
    @param offsets: 2x4 array of time offsets for each hydrophone
    @param hydro_xy: 8x2 array of hydrophone locations
    """
    triads = [(0, 1, 2), (1, 2, 3), (2, 3, 0), (3, 0, 1)]
    times_q0 = times[0,target,:] + offsets[0,:]
    times_q1 = times[1,target,:] + offsets[1,:]
    xy_q0 = hydro_xy[0:4,:]
    xy_q1 = hydro_xy[4:8,:]
    results = numpy.zeros((2, len(triads), 3))
    for i, sel in enumerate(triads):
        x, y, z = spherical_xyz(times_q0, xy_q0, list(sel))
        results[0, i, :] = x, y, z
        x, y, z = spherical_xyz(times_q1, xy_q1, list(sel))
        results[1, i, :] = x, y, z
    return results

def ranges(target, times, offsets, delay=2.2e-3, sound_speed=1574.):
    times_q0 = times[0,target,:] + offsets[0,:]
    times_q1 = times[1,target,:] + offsets[1,:]
    ranges_q0 = (times_q0 - delay)*sound_speed
    ranges_q1 = (times_q1 - delay)*sound_speed
    return ranges_q0, ranges_q1

def process(datafile, xyfile):
    f = open(xyfile, 'r')
    locs = []
    for line in f:
        locs.append([float(p) for p in line.strip().split(',')])
    hydro_xy = numpy.array(locs)
    pylab.plot(hydro_xy[:,0], hydro_xy[:,1], 'k+')
    pylab.hold(True)
    f.close()
    f = open(datafile, 'r')
    line = f.next()
    record, offsets = line.strip().split('|')
    tstamp, times = parse_record(record)
    times *= 1.0e-4
    offsets = numpy.array([float(x) for x in offsets.split()]).reshape(2, 4)
    r0, r1 = ranges(1, times, offsets)
    ax = pylab.gca()
    ax.set_xlim([-10000, 10000])
    ax.set_ylim([-10000, 10000])
    ax.set_aspect('equal')
    ax.grid(True)
    for i, xy in enumerate(hydro_xy[0:4,:]):
        e = Ellipse(xy=xy, width=r0[i], height=r0[i])
        ax.add_artist(e)
        e.set_clip_box(ax.bbox)
        e.set_alpha(0.60)
        e.set_facecolor(rand(3))
    for i, xy in enumerate(hydro_xy[4:8,:]):
        e = Ellipse(xy=xy, width=r1[i], height=r1[i])
        ax.add_artist(e)
        e.set_clip_box(ax.bbox)
        e.set_alpha(0.60)
        e.set_facecolor(rand(3))
        
#     results = track_target(1, times, offsets, hydro_xy)
#     print results
#     pylab.plot(results[0,:,0], results[0,:,1], 'k.')
#     pylab.plot(results[1,:,0], results[1,:,1], 'r.')
        
if __name__ == '__main__':
    import sys
    phones = numpy.array([[416.83, -153.61, 33.33334],
                          [-189.53, 418.99, 33.33334],
                          [-466.13, -43.51, 33.33334]])
    times = numpy.array([4.25705,
                         4.59430,
                         4.80177])
    c = numpy.array([1570., 1570., 1570.])
    valid, t1, t2 = spherical_xyz(times, phones, c)
    print t1, t2
    #process(sys.argv[1], sys.argv[2])
    #pylab.show()
