#!/usr/bin/env python
#
# Reformat a tracking log file to make it easier to plot the timestamps.
#
import sys
import time

def to_seconds(dt_str):
    """
    Convert a date/time string of the form MM/DD/YYYY HH:MM:SS
    to seconds since 1/1/1970.
    """
    return time.mktime(time.strptime(dt_str, '%m/%d/%Y %H:%M:%S'))

def main(args):
    try:
        infile = open(args[0], 'r')
    except IndexError:
        infile = sys.stdin

    t0 = None
    for line in infile:
        f = line.strip().split()
        t = to_seconds('%s %s' % (f[0], f[1]))
        if t0 is None:
            t0 = t
        t = t - t0
        print ' '.join([str(t)] + f[2:])

if __name__ == '__main__':
    main(sys.argv[1:])
    
