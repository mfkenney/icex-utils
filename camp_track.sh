#!/bin/bash
#
# Usage: camp_track.sh start_time end_time
#
if which gdate 1> /dev/null 2>&1
then
    DATE=gdate
else
    DATE=date
fi

: ${DB_SERVER=aplis-survey.local}
: ${DB_NAME=raw_gps}

usage ()
{
    cat<<EOF
Usage: $(basename $0) start_time end_time

Extract the GPS track of the Camp between START_TIME and END_TIME. Both
time specifications are in any format allowed by the GNU date command.
Output is written to standard out in KML format.
EOF
    exit 1
}

if [ $# -lt 2 ]
then
    usage
fi

if [ "$DB_NAME" = "orientation" ]
then
    style="aplis_orientation.style"
else
    style="aplisgps.style"
fi

t_start=$($DATE -d "$1" +%s)
t_end=$($DATE -d "$2" +%s)

curl --silent -X GET \
  "http://$DB_SERVER:5984/$DB_NAME/_design/trackapp/_list/as-csv/interval_10s?startkey=\[$t_start\]&endkey=\[$t_end\]" |\
  grep origin |\
  gpsbabel -i xcsv,style=$HOME/.gpsbabel/${style} -f /dev/stdin\
   -o kml,points=0,line_color=ff0000aa,track=1,trackdata=0,trackdirection=1 -F /dev/stdout
