#!/usr/bin/env python
#
# Read counter values from a serial port as output by the Track program and emit
# them as signals on the D-bus.
#
import dbus
import dbus.service
import gobject
from dbus.exceptions import DBusException
import dbus.mainloop.glib
import sys
import serial
import time

class Emitter(dbus.service.Object):
    def __init__(self, bus):
        dbus.service.Object.__init__(self, bus, '/Counters')
        self.bus = bus

    @dbus.service.signal(dbus_interface='icex.ICounters',
                         signature='uai')
    def detect(self, t, counters):
        pass

def to_seconds(dt_str):
    """
    Convert a date/time string of the form MM/DD/YYYY HH:MM:SS
    to seconds since 1/1/1970.
    """
    return time.mktime(time.strptime(dt_str, '%m/%d/%Y %H:%M:%S'))

def read_records(input, eol='\n'):
    """
    Generator to return the next timer record from the input port
    """
    buf = []
    while True:
        c = input.read(1)
        if c == '':
            raise StopIteration
        buf.append(c)
        if c == eol:
            yield ''.join(buf)
            buf = []
            
def split_record(rec):
    """
    Split a timer record string into a tuple consisting of a timestamp in seconds
    since 1/1/1970 and a list of integer counter values.
    """
    fields = rec.strip().split()
    t = to_seconds('%s %s' % tuple(fields[0:2]))
    counters = map(int, fields[2:])
    return t, counters

def process_records(source, emitter):
    for rec in read_records(source):
        try:
            timestamp, counters = split_record(rec)
            emitter.detect(timestamp, counters)
        except (ValueError, IndexError):
            pass

def main(args):
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    bus = dbus.SessionBus()
    name = dbus.service.BusName("icex.Counters", bus)
    source = serial.Serial('/dev/ttyS0', baudrate=115200, timeout=5)
    emitter = Emitter(bus)
    process_records(source, emitter)

if __name__ == '__main__':
    main(sys.argv[1:])
