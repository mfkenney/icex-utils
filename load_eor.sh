#!/bin/bash
#
# Use gpsbabel to load the EOR location into a Garmin GPS.
#
PATH=/bin:/usr/bin:/usr/local/bin:/opt/local/bin
export PATH

set -e

if [ $# -lt 2 ]
then
    echo "Usage: $(basename $0) DD-MM.mmmH DDD-MM.mmmH [devicename]"
    echo ""
    echo "Load the EOR location into a Garmin GPS connected to a serial port"
    exit 1
fi

# Input is DEGREES-MINUTES[N,S,E,W] but we need to convert to the format
# expected by the Garmin 'dmm' grid.
#lat=$(echo $1|sed -n -e 's/\([0-9]*\)-\([0-9\.]*\)\([N,S]\)/\3\1 \2/p')
#lon=$(echo $2|sed -n -e 's/\([0-9]*\)-\([0-9\.]*\)\([E,W]\)/\3\1 \2/p')
lat="$1"
lon="$2"
if [ -n "$3" ]
then
    devname="$3"
else
    devname="/dev/ttyS0"
fi

# Create a Universal CSV file for input and send it to the GPS
cat<<EOF | gpsbabel -w -i unicsv,grid=ddd -f /dev/stdin -o garmin -F "$devname"
Name,Latitude,Longitude,Alt
EOR,$lat,$lon,0
EOF
