Tracking Range Survey
=====================

The APLIS tracking range X-Y coordinate system is a rotated Transverse Mercator
projection defined by an origin latitude/longitude and the azimuth of the Y
axis measured clockwise from true north. In the examples below ``$LAT0/$LON0``
is the origin and ``$AZ`` is the azimuth.

Conversions
-----------

The PROJ.4 Cartographic Projections Library utilities can be used to convert
between latitude-longitude and East-North coordinates. Separate software will
need to be used to perform the rotation, a simple ``gawk`` (GNU awk) script is used in
the examples below.

Forward Projection
~~~~~~~~~~~~~~~~~~

``Proj`` converts to East-North and ``gawk`` converts to X-Y. The ``+units``
option is used to force the output units to yards (the default is meters)::

    echo "$LON $LAT" | proj +proj=tmerc +ellps=WGS84 +units=us-yd +lat_0=$LAT0 +lon_0=$LON0|\
     gawk -v az="$AZ" 'BEGIN {az_r = az*atan2(1, 1)*4./180.;\
                           c_a = cos(az_r); s_a = sin(az_r); } \
              function calc_x(e, n) { return e*c_a - n*s_a }\
              function calc_y(e, n) { return n*c_a + e*s_a }\
              {printf "%.2f %.2f\n", calc_x($1,$2), calc_y($1,$2)}'

Inverse Projection
~~~~~~~~~~~~~~~~~~

Rotate X-Y into the East-North coordinate system and then use ``invproj`` to
convert to latitude-longitude (Note that the first output value is longitude
because it is the "x" axis).  Use the ``-f`` option to force the ``invproj``
output to be in decimal degrees. By default the output is in DMS format::

  echo "$X $Y" | gawk -v az="$AZ" 'BEGIN {az_r = (360-az)*atan2(1, 1)*4./180.;\
                         c_a = cos(az_r); s_a = sin(az_r);} \
              function calc_e(x, y) { return x*c_a - y*s_a }\
              function calc_n(x, y) { return y*c_a + x*s_a }\
              {printf "%.2f %.2f\n", calc_e($1,$2), calc_n($1,$2)}'|\
  invproj +proj=tmerc +ellps=WGS84 +units=us-yd +lat_0=$LAT0 +lon_0=$LON0 -f "%.6f"

Lake Test Survey
----------------

#. Install hydrophones as follows:

  * H1: port side, forward
  * H2: port side, aft
  * H3: starboard side, aft
  * H4: starboard side, forward

#. Perform hydrophone survey. Leave origin at H4 and Y-axis aligned with H1-H2.
#. Obtain GPS fix of H4 location, this will be the projection origin (``$LAT0``,
   ``$LON0``).
#. Obtain ship's true heading, this will be the projection azimuth ``$AZ``.

Since the Barge will be in a 3 (or 4) point moor, we can assume that the
origin and azimuth will not change.

Converting GPS track to xy
~~~~~~~~~~~~~~~~~~~~~~~~~~

Use ``gpsbabel`` to download track from handheld GPS and save in GPX format::

  gpsbabel -i garmin -f /dev/cu.KeySerial1 -t -o gpx -F $GPXFILE

Use the ``aplis_xy.sh`` script to convert to grid coordinates::

  aplis_xy.sh gpx $GPXFILE $LAT0,$LON0,$AZ > $TRKFILE

Tracking Grid Rotation
----------------------

This requires that a GPS unit be located at two known grid locations. The true
azimuth of the grid is the difference between the GPS and grid azimuths
between the two points::

  AZ = AZ_true - AZ_rel



Range Survey
--------------

#. Collect travel time data using each hydrophone as a transmitter in turn
#. Process the travel times using the survey program to produce the initial
   hydrophone coordinates. H4 will be the origin and the Y-axis will be parallel
   to the line between H1 and H2. Create initial survey.xyz file.
#. Load the initial survey.xyz file into Track and transmit from a
   pinger located in the Command Hut hydro-hole. This will be the new grid
   origin.
#. Use the survey program to translate the grid to the new origin.
#. Calculate the true heading of the Y-axis using the GPS coordinates of the
   origin and the GPS coordinates of one of the hydrophones.
#. Use the survey program to rotate the grid so that the Y-axis is aligned
   with true north.
#. Create the final survey.xyz file and load into Track
