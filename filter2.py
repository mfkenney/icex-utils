#!/usr/bin/env python
#
# Example Kalman filter for GPS positions. Runs the filter on the ECEF coordinates and
# also produces a velocity estimate.
#
import numpy
import sys
import math
import json
import pyproj
from datetime import datetime
from multiprocessing import Process, Queue
from dateutil.tz import tzutc
import kalman

# The constants a and b are from http://users.erols.com/dlwilson/gpshdop.htm
def rms_horiz_error(hdop, a=3.07, b=3.57):
    return math.sqrt((a*hdop)**2 + b**2)

wgs84_lla = pyproj.Proj(proj='latlon', datum='WGS84', ellps='WGS84')
wgs84_ecef = pyproj.Proj(proj='geocent', datum='WGS84', ellps='WGS84')

def to_ecef(lat, lon, alt=0.):
    return numpy.array(pyproj.transform(wgs84_lla, wgs84_ecef, lon, lat, alt))

def to_lla(x, y, z):
    lon, lat, alt = pyproj.transform(wgs84_ecef, wgs84_lla, x, y, z)
    return lat, lon, alt

def total_seconds(td):
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 1e6)/1e6

METERS_PER_DEG = 60*1852.

HDOP = 1.5
h_error = rms_horiz_error(HDOP)

class FilterEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, kalman.Filter):
            return obj.as_dict()
        return json.JSONEncoder.default(self, obj)

def kalman_filter(inqueue, outqueue, initial_state):
    f = kalman.Filter()
    di = numpy.diag_indices(6)
    f.R[di] = initial_state['rms_error']
    t = initial_state['time']
    f.xhat = numpy.matrix(initial_state['state']).T
    next_state = inqueue.get()
    while next_state:
        dt = next_state['time'] - t
        t = next_state['time']
        secs = total_seconds(dt)
        f.A[0,3] = secs
        f.A[1,4] = secs
        f.A[2,5] = secs
        if 'state' in next_state:
            f.z = numpy.matrix(next_state['state']).T
        else:
            f.z = f.xhat.copy()
        rms_error = next_state.get('rms_error')
        if rms_error:
            f.R[di] = next_state['rms_error']
        f.advance()
        outqueue.put(f.xhat)
        next_state = inqueue.get()

def make_transform_matrix(lat, lon):
    lat = math.radians(lat)
    lon = math.radians(lon)
    s_lat = math.sin(lat)
    c_lat = math.cos(lat)
    s_lon = math.sin(lon)
    c_lon = math.cos(lon)
    return numpy.matrix([[-s_lon, c_lon, 0.],
                         [-s_lat*c_lon, -s_lat*s_lon, c_lat],
                         [-c_lat*c_lon, -c_lat*s_lon, -s_lat]])

def main():
    state = numpy.zeros(6)
    infile = open(sys.argv[1], 'r')
    t, lat, lon = infile.next().strip().split()
    state[0:3] = to_ecef(float(lat), float(lon))
    outqueue = Queue()
    inqueue = Queue()
    rms_error = numpy.ones(6)*(h_error**2)
    rms_error[3] = 2.*rms_error[3]/10000.
    rms_error[4] = 2.*rms_error[4]/10000.
    rms_error[5] = 2.*rms_error[5]/10000.
    p = Process(target=kalman_filter, args=(outqueue,
                                            inqueue,
                                            {'state': state,
                                             'rms_error': rms_error,
                                             'time': datetime.strptime(t, '%Y-%m-%dT%H:%M:%SZ').replace(tzinfo=tzutc())}))
    p.start()
    for line in infile:
        t, lat, lon = line.strip().split()
        state[0:3] = to_ecef(float(lat), float(lon))
        outqueue.put({'state': state,
                      'time': datetime.strptime(t, '%Y-%m-%dT%H:%M:%SZ').replace(tzinfo=tzutc())})
        estimate = inqueue.get()
        latp, lonp, alt = to_lla(estimate[0,0], estimate[1,0], estimate[2,0])
        xform = make_transform_matrix(latp, lonp)
        vel = xform*estimate[3:6,0]
        state[3:6] = vel.T[0,:]
        v = math.sqrt(vel[0,0]**2 + vel[1,0]**2)
        heading = math.degrees(math.atan2(vel[0,0], vel[1,0]))
        if heading < 0:
            heading += 360.
        print t, latp, lonp, v, heading

    outqueue.put({})
    p.join()

if __name__ == '__main__':
    main()
