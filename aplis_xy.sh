#!/bin/bash
#
# Given a file of GPS positions (a track) in any format supported by Gpsbabel, output a
# file of X and Y coordinates on the APLIS grid.
#
PROG="$0"

usage ()
{
    cat<<EOF 1>&2
Usage: $PROG filetype filename lat0,lon0,az

Time, X, and Y (in meters) is written to stdout. lat0, lon0, and az define
the grid projection.
EOF
}

if [ $# -lt 3 ]
then
    usage
    exit 1
fi

intype="$1"
infile="$2"
params="$3"

save_ifs="$IFS"
IFS=","
set -- $params
IFS="$save_ifs"
lat0="$1"
lon0="$2"
az="$3"

#
# Extract the contents of the track file and use PROJ to convert to grid coordinates. PROJ does define an
# Oblique Mercator projection but there seems to be a problem with the rotation calculation when the 
# y-axis azimuth lies between 270 and 360 degrees. Therefore, I use the Transverse Mercator projection
# and rotate using the following equations:
#
#    x = E*cos($AZ) - N*sin($AZ)
#    y = N*cos($AZ) + E*sin($AZ)
#
# Where E and N are the east (x) and north (y) coordinates returned by the Transverse Mercator projection.
#
gpsbabel -i $intype -f "$infile" -o xcsv,style=$HOME/.gpsbabel/gpslog.style -F /dev/stdout |\
while read tstamp lat lon
do
    echo "$lon $lat $tstamp"
done | proj +proj=tmerc +ellps=WGS84 +units=us-yd +lat_0=$lat0 +lon_0=$lon0 |\
  gawk -v az="$az" 'BEGIN {az_r = az*atan2(1, 1)*4./180.;\
                           c_a = cos(az_r); s_a = sin(az_r);} \
              function calc_x(e, n) { return e*c_a - n*s_a }\
              function calc_y(e, n) { return n*c_a + e*s_a }\
              {printf "%.2f %.2f %s\n", calc_x($1,$2), calc_y($1,$2), $3}'
